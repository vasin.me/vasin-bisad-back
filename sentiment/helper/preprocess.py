import pandas as pd
import numpy as np
import os, json, sys
import copy, glob
import pythainlp
import re
from sklearn.feature_extraction.text import TfidfVectorizer

def text_to_tokens(text, dict_trie=None, stop_words=[]):
    # nomalize
    text = pythainlp.util.normalize(text)

    # remove duplicate ending characters
    for m in re.finditer(r'([\u0E00-\u0E7F])(\1{2,})', text):
        text = text.replace(m.group(0),m.group(1),1)
        
    # remove html tag
    cleaner = re.compile('<.*?>')
    text = re.sub(cleaner, '', text)

    # remove url
    text = re.sub(r'http\S+', '', text)
    
    # remove unwanted character
    pattern = re.compile(r"[^\u0E00-\u0E7Fa-zA-Z ]|ๆ")
    text = pattern.sub('',text)
    
    # lowercase
    text = text.lower()
    
    # tokenization
    if dict_trie is None:
        tokens = pythainlp.tokenize.word_tokenize(text, engine='newmm')
    else:
        tokens    = pythainlp.tokenize.dict_word_tokenize(text, dict_trie)
        tokens    = list(filter(lambda t: ' ' not in t and '\n' not in t, tokens))
    
    # remove stopwords
    tokens = [t for t in tokens if not t in stop_words] 
    
    # single character 
    tokens = [t for t in tokens if len(t.replace(' ', '')) > 1] 
    return tokens

def join_tokens(text):
    return ','.join(text)



def word_score(text):

    tfidf = TfidfVectorizer(analyzer=lambda x: x.split(','))
    tokenize = text_to_tokens(text)
    text = join_tokens(tokenize)
    tfidf.fit_transform(text)

    response = tfidf.transform(text).toarray()
    features = tfidf.get_feature_names()

    tmp = response.max(axis=0)
    lst = []
    for idx, val in enumerate(tmp):
        lst.append([features[idx], val, tfidf.vocabulary_[features[idx]]])

    return sorted(lst, key=lambda x: -x[1])