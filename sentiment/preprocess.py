import pandas as pd
import numpy as np
import multiprocessing as mp

from tqdm import tqdm
from glob import glob
from helper import text_to_tokens as helper

def text_tokenize(df, target):
    """
    df: dataframe
    target: column you want to tokenize
    """
#     paths = glob("../data/training/*.csv")
    
#     # read all dataframe
#     print('Loading data...')
#     df = pd.DataFrame()
#     for path in tqdm(paths):
#         tmp = pd.read_csv(path)
#         df = df.append(tmp)
#     df = df.drop_duplicates()

    pool = mp.Pool(processes=2)
    
#     print('Loading data to thread')
    results = [pool.apply_async(helper.text_to_tokens, args=(v,)) for v in df.message.values]
    
#     print('Process...')
    word_tk = [p.get() for p in results]

    word_tk = np.array(word_tk)
#     np.save('../data/tokenize/training_tokenize_v1.npy', word_tk)
#     print('Complete!')
    
    return word_tk
