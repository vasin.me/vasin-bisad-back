"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('campaign/', include('campaign.urls')),
    # path('competitor/', include('competitor.urls')),
    path('Customer/', include('Customer.urls')),
    path('Marketing/', include('Marketing.urls')),
    path('Executive/', include('Executive.urls')),

    # api 
    path('test/', views.show), # test query
    path('upload/', views.upload), # upload data
    path('trend/', views.getTrend),
    path('find/', views.findKeyword), # find keyword
    path('track/', views.track),
    path('campaign/', views.campaign) 
]
