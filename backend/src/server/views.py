import json
import datetime
import requests as req
import firebase_admin
import pandas as pd
import numpy as np
import ast

from firebase_admin import credentials, firestore
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from sklearn.feature_extraction.text import TfidfVectorizer

from utilities import preprocess

# setup api
cred = credentials.Certificate('./key/bisad-300c1-firebase-adminsdk-dqw30-39d92da570.json')
firebase_admin.initialize_app(cred)
db = firestore.client()

limit_query = 100

def loadData(filter_):
    docs = db.collection('Social').order_by('datetime', 
        direction=firestore.Query.DESCENDING).limit(limit_query).get()
    date = str(datetime.datetime.utcnow())
    res = []
    for doc in docs:
        doc = doc.to_dict()
        # filter
        if isinstance(filter_, list):
            if any(fil.lower() in doc['message'].lower() for fil in filter_):
                res.append(doc)
        if isinstance(filter_, str):
            if filter_.lower()  in doc['message'].lower():
                res.append(doc)
        if filter_ == None:
            res.append(doc)

    return (res, date)

def show(request, filter_ = None):
    '''get all document
    '''
    docs, date = loadData(filter_)

    # print(len(response))

    print('-'*50)
    print('---- Query at:', date, '----')
    print('-'*50)

    return JsonResponse({
        "result": docs
    })

@csrf_exempt
def upload(request):
    '''upload text to server
    
    request format:
        {
            "message": xxx,
            "message_tag": xxx,
            "message_type": xxx,
            "linktosource": xxx,
        }
    '''

    # print(request.body)
    data = json.loads(request.body)
    date = str(datetime.datetime.utcnow())
    # print(data)

    db.collection('Social').document(date).set({
        "datetime": date,
        "message": data['message'],
        "tokenize": preprocess.text_to_tokens(data['message']),
        "linktosource": data['linktosource'],
        'message_type': data['message_type'],
        'message_tag': list(data['message_tag'].split(','))
    })

    print('-'*50)
    print('---- Upload at:', date, '----')
    print('-'*50)

    return HttpResponse("OK")


def getTrend(request, filter_ = None):
    
    try:
        data = json.loads(request.body)
        filter_ = data['filter']
    except:
        pass
    docs, date = loadData(filter_)

    # ----- TF-IDF ----- #
    df = pd.DataFrame(docs)

    training = df['tokenize']
    tfidf = TfidfVectorizer(analyzer=lambda x: x.split(','))
    tfidf.fit(training)

    tfidf_res = tfidf.transform(training)

    tfidf_sc = []
    score = np.max(tfidf_res, axis=0).toarray()
    # score = tfidf_res
    for w, k in tfidf.vocabulary_.items():
        tfidf_sc.append([w, score[0, k]])

    res = sorted(tfidf_sc, key=lambda x: -x[1])
    # ----- End ----- #

    print('-'*50)
    print('---- Load Trend at:', date, '----')
    print('-'*50)

    return JsonResponse({
        "Result": res
    })

@csrf_exempt
def findKeyword(request, filter_ = None):
    '''find keyword
    
    request format:
        {
            "keyword": xxx
        }
    '''

    data = json.loads(request.body)
    docs, date = loadData(filter_)

    keyword = data['keyword']
    
    res = []
    for doc in docs:
        if isinstance(keyword, list):
            for k in keyword:
                if k.lower() in doc['message'].lower():
                    res.append(doc)
        if isinstance(keyword, str):
            if keyword.lower() in doc['message'].lower():
                res.append(doc)

    print('-'*50)
    print('---- Find Keyword at:', date, '----')
    print('-'*50)

    return JsonResponse({
        "result": res
    })

@csrf_exempt
def track(request, filter_ = None):
    '''load tracking feed
    
    request format:
        {
            role: xxx
        }
    '''
    lst_role = {
        "customer": 'http://127.0.0.1:8000/Customer/track',
        "marketing": 'http://127.0.0.1:8000/Marketing/track',
        "executive": 'http://127.0.0.1:8000/Executive/track',
    }

    data = json.loads(request.body)
    try:
        url = lst_role[data['role'].lower()]
    except:
        return HttpResponse("Please choose customer or marketing or executive.")

    r = req.get(url)
    r = ast.literal_eval(r.content.decode("utf-8"))

    docs, date = loadData(filter_)

    res = []
    for doc in docs:
        for k in r:
            if k['track_name'].lower() in doc['message'].lower():
                res.append(doc)
                break
    
    print('-'*50)
    print('---- Load Track at:', date, '----')
    print('-'*50)
    
    return JsonResponse({
        "result": res
    })

@csrf_exempt
def campaign(request, filter_ = None):
    '''load campaign feed
    
    request format:
        {
            role: xxx
        }
    '''
    lst_role = {
        "customer": 'http://127.0.0.1:8000/Customer/campaign',
        "marketing": 'http://127.0.0.1:8000/Marketing/campaign',
        "executive": 'http://127.0.0.1:8000/Executive/campaign',
    }

    data = json.loads(request.body)
    try:
        url = lst_role[data['role'].lower()]
    except:
        return HttpResponse("Please choose customer or marketing or executive.")

    r = req.get(url)
    r = ast.literal_eval(r.content.decode("utf-8"))

    docs, date = loadData(filter_)

    res = []
    for doc in docs:
        for k in r:
            if k['campaign_name'].lower() in doc['message'].lower():
                res.append(doc)
                break
    
    print('-'*50)
    print('---- Load Campaign at:', date, '----')
    print('-'*50)
    
    return JsonResponse({
        "result": res
    })


    
    