from django.contrib import admin
from .models import Track, Campaign

# Register your models here.
admin.site.register(Track)
admin.site.register(Campaign)