from django.db import models

# Create your models here.
class Track(models.Model):
    track_name = models.CharField(max_length=50)

    def __str__(self):
        return self.track_name

class Campaign(models.Model):
    campaign_name = models.CharField(max_length=50)

    def __str__(self):
        return self.campaign_name