from django.shortcuts import render
from rest_framework import viewsets, permissions
from .models import competitor
from .serializers import CompetitorSerializer

# Create your views here.
class CompetitorView(viewsets.ModelViewSet):
    queryset = competitor.objects.all()
    serializer_class = CompetitorSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )