from rest_framework import serializers
from .models import competitor

class CompetitorSerializer(serializers.ModelSerializer):
    class Meta:
        model = competitor
        fields = ('id', 'company_name', 'age')