from rest_framework import viewsets, permissions
from .models import Track, Campaign
from .serializers import TrackSerializer, CampaignSerializer

# Create your views here.
class TrackView(viewsets.ModelViewSet):
    queryset = Track.objects.all()
    serializer_class = TrackSerializer
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly, )

class CampaignView(viewsets.ModelViewSet):
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly, )