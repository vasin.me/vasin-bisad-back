from django.shortcuts import render
from rest_framework import viewsets, permissions
from .models import tracking, setting
from .serializers import TrackingSerializer, SettingSerializer

# Create your views here.
class TrackingView(viewsets.ModelViewSet):
    queryset = tracking.objects.all()
    serializer_class = TrackingSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )

class SettingView(viewsets.ModelViewSet):
    queryset = setting.objects.all()
    serializer_class = SettingSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )