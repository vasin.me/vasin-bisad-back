from django.contrib import admin
from .models import tracking, setting

# Register your models here.
admin.site.register(tracking)
admin.site.register(setting)