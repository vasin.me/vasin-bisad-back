from rest_framework import serializers
from .models import tracking, setting

class TrackingSerializer(serializers.ModelSerializer):
    class Meta:
        model = tracking
        fields = ('id', 'name', 'age')

class SettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = setting
        fields = ('id', 'option')