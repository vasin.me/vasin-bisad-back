from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('tracking', views.TrackingView)
router.register('setting', views.SettingView)

urlpatterns = [
    path('', include(router.urls))
]