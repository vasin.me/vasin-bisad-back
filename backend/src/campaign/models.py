from django.db import models

# Create your models here.
class tracking(models.Model):
    name = models.CharField(max_length=50)
    age  = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class setting(models.Model):
    option = models.CharField(max_length=50)

    def __str__(self):
        return self.option