from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('track', views.TrackView)
router.register('campaign', views.CampaignView)

urlpatterns = [
    path('', include(router.urls))
]