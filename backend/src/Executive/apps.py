from django.apps import AppConfig


class ExecutiveConfig(AppConfig):
    name = 'Executive'
