# API Document



### request format

|        Route          |                Description             |        Request       |
| --------------------- | -------------------------------------- | -------------------- |
| `/getTrend/`          | Get trend now                          | `None`               |
| `/getRealtime/`       | Get real time db                       | { <br> &nbsp;(optional) filter: `list` or `string` <br> } |
| `/getTrackingName/`   | Get tracking name                      | `None` |
| `/getTrackingFeed/`   | Get real time db with track            | { <br> &nbsp;brand: `string` <br>}               |
| `/getSummary/`        | Get summary                            | { <br> &nbsp;brand: `string` <br>} |
| `/insertTrackingName/`    | Insert tracking name                   | { <br> &nbsp;track_name: `string` <br> &nbsp;role: `string` <br>} |
| `/insertRealtime/`    | Insert real time db                    | { <br> &nbsp; message: `string` <br> &nbsp; message_tag: `string separate with comma(,)` <br> &nbsp; message_type: `string('topic', 'comment')` <br> &nbsp; linktosource: `string(url)` <br>} |
| `/updateRealtime/`   | Update reply                            | { <br> &nbsp; doc: `object` <br> } |
| `/summaryAll/`       | Save summary                            | `None`               |
| `/removeTrackingName/`| Remove tracking name                   | { <br> &nbsp;track_name: `string` <br> &nbsp;role: `string` <br>}
