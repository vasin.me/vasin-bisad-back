#!/usr/bin/env python3

import pymysql.cursors
import json
import datetime
import pandas as pd
import numpy as np
import firebase_admin

from glob import glob
from flask_cors import CORS
from flask import Flask, request
from firebase_admin import credentials, firestore
from sklearn.feature_extraction.text import TfidfVectorizer
from tqdm import tqdm
from os import getenv
# from dotenv import load_dotenv
# from pathlib import Path

from utils import preprocess

app = Flask(__name__)
CORS(app)
# env_path = Path('.')
# load_dotenv(dotenv_path=env_path)

print(getenv('MYSQL_HOST'))

# Connect to the database
connection = pymysql.connect(
    host= getenv('MYSQL_HOST') or 'localhost',
    user= getenv('MYSQL_USER') or 'root',
    password= getenv('MYSQL_PASSWORD') or 'password',
    db= getenv('MYSQL_DATABASE') or 'bisad',
    cursorclass=pymysql.cursors.DictCursor
)

# Setup firebase
cred = credentials.Certificate('./key/bisad-300c1-firebase-adminsdk-dqw30-39d92da570.json')
firebase_admin.initialize_app(cred)
db = firestore.client()

limit_query = 100

def loadtext(path):
    with open(path, encoding="utf8") as f:
        txt = f.readlines()
        txt = list(set(list(map(lambda x: x.strip(), txt))))
        return txt

# Load alias name
AIS_word  = loadtext('./utils/data/alias/AIS.txt')
DTAC_word = loadtext('./utils/data/alias/DTAC.txt')
TRUE_word = loadtext('./utils/data/alias/TRUE.txt')

# Load sentiment word
pos_word = loadtext('./utils/data/pos_neg/pos.txt')
neg_word = loadtext('./utils/data/pos_neg/neg.txt')

# Load intention
intentions = []
paths = glob('./utils/data/intention/*.txt')
for path in paths:
    intentions.append(loadtext(path))

"""
.##.....##.########.####.##........######.
.##.....##....##.....##..##.......##....##
.##.....##....##.....##..##.......##......
.##.....##....##.....##..##........######.
.##.....##....##.....##..##.............##
.##.....##....##.....##..##.......##....##
..#######.....##....####.########..######.
"""

def __sentiment(message):
    
    idx2sentiment = { idx: sentiment for idx, sentiment in zip(range(2), ['Positive', 'Negative']) }
    
    pos_sc, neg_sc, neutral_sc = 0, 0, 0
    pos_sc     = sum(w in message for w in pos_word)
    neg_sc     = sum(w in message for w in neg_word)
    total      = pos_sc + neg_sc if pos_sc + neg_sc != 0 else 1
    pos_per    = pos_sc/total
    neg_per    = neg_sc/total
    
    return idx2sentiment[np.argmax([pos_sc, neg_sc])], pos_sc, neg_sc

def __getBrand(message):
    result = []
    for word in AIS_word:
        if word.lower() in message.lower():
            result.append('AIS')

    for word in DTAC_word:
        if word.lower() in message.lower():
            result.append('DTAC')

    for word in TRUE_word:
        if word.lower() in message.lower():
            result.append('TRUE')
    
    return result

def __getIntention(message):
    result = []
    for intention in intentions:
        for word in intention:
            if word.lower() in message.lower():
                result.append(word[0])
                break
    return result

def __getRequest():
    try:
        req = json.loads(request.data.decode('utf-8'))
        return req
    except:
        return None

def __getFromDB(table_name): # get data with specific db
    
    cursor = connection.cursor()
    sql = "SELECT * FROM `{}`".format(table_name)
    cursor.execute(sql)
    result = cursor.fetchall()
    cursor.close()
    return result

def __getRealtime(filter_ = None): # get data from real-time db
    docs = db.collection('Social').order_by('datetime', 
        direction=firestore.Query.DESCENDING).limit(limit_query).get()
    res = []

    def append(doc):
        # add brand
        brand_lst = __getBrand(doc['message'])

        # add intention
        intention_lst = __getIntention(doc['message'])

        # add sentiment
        sentiment, pos_per, neg_per = __sentiment(doc['message'])

        doc['brand'] = brand_lst
        doc['intention'] = intention_lst
        doc['sentiment'] = sentiment
        doc['pos_percentage'] = pos_per
        doc['neg_percentage'] = neg_per
        
        res.append(doc)

    for doc in docs:
        doc = doc.to_dict()
        # filter
        if isinstance(filter_, list):
            if any(fil.lower() in doc['message'].lower() for fil in filter_):
                append(doc)
        if isinstance(filter_, str):
            if filter_.lower()  in doc['message'].lower():
                append(doc)
        if isinstance(filter_, dict):
            if any(fil.lower() in doc['message'].lower() for fil in filter_['filter']):
                append(doc)
        if filter_ == None:
            append(doc)
    return res

def __getTrend(filter_ = None):
    docs = __getRealtime(filter_)
    df = pd.DataFrame(docs)

    training = df['tokenize']
    tfidf = TfidfVectorizer(analyzer=lambda x: x.split(','))
    tfidf.fit(training)

    tfidf_res = tfidf.transform(training)

    tfidf_sc = []
    score = np.max(tfidf_res, axis=0).toarray()
    # score = tfidf_res
    for w, k in tfidf.vocabulary_.items():
        tfidf_sc.append([w, score[0, k]])

    res = sorted(tfidf_sc, key=lambda x: -x[1])
    # ----- End ----- #

    return res[:100]

"""
.########...#######..##.....##.########.########
.##.....##.##.....##.##.....##....##....##......
.##.....##.##.....##.##.....##....##....##......
.########..##.....##.##.....##....##....######..
.##...##...##.....##.##.....##....##....##......
.##....##..##.....##.##.....##....##....##......
.##.....##..#######...#######.....##....########
"""

@app.route('/getTrend', methods=['GET', 'POST'])
def getTrend():
    return json.dumps({
        "result": __getTrend()
    })

@app.route('/getRealtime', methods=['GET', 'POST'])
def getRealtime():
    '''
        {
            (optional) filter: list, or string
        }
    '''
    req = __getRequest()
    return json.dumps({
        "result": __getRealtime(req)
    })

@app.route("/getTrackingName", methods=['GET', 'POST'])
def getTrackingName():
    result = __getFromDB('Tracking')
    return json.dumps({
        "result": result
    })

@app.route("/getTrackingFeed", methods=['GET', 'POST'])
def getTrackingFeed():
    '''
        {
            brand: xxx
        }
    '''
    req = __getRequest()
    cursor = connection.cursor()
    sql = "SELECT * FROM `Tracking` WHERE `role` = %s"
    cursor.execute(sql, [req['brand']])
    track_name = cursor.fetchall()
    # print(track_name)
    tmp = list(map(lambda x: x['track_name'], track_name))
    # print(tmp)
    docs = __getRealtime(tmp)
    cursor.close()
    return json.dumps({
        "result": docs
    })

@app.route('/getSummary', methods=['GET', 'POST'])
def getSummary():
    '''
        {
            brand: xxx
        }
    '''
    req = __getRequest()
    docs = __getFromDB('Summary')
    res = []
    for doc in docs:
        if doc['brand'].lower() == req['brand'].lower():
            res.append(doc)
    return json.dumps({
        "result": res
    })

@app.route("/removeTrackingName", methods=['GET', 'POST'])
def removeTrackingName():
    '''
        {
            track_name: xxx,
            role: xxx
        }
    '''
    req = __getRequest()
    cursor = connection.cursor()
    sql = 'DELETE FROM `Tracking` WHERE `track_name` = %s AND `role` = %s'
    cursor.execute(sql, [req['track_name'], req['role']])
    cursor.close()
    connection.commit()
    return 'ok'

@app.route("/insertTrackingName", methods=['GET', 'POST'])
def insertTrackingName():
    '''
        {
            track_name: xxx,
            role: xxx
        }

    '''
    req = __getRequest()
    docs = __getFromDB('Tracking')
    for doc in docs:  # check duplicated
        if doc['track_name'] == req['track_name'] and \
            doc['role'] == req['role']:
            return "duplicate"
    try:
        cursor = connection.cursor()
        sql = "INSERT INTO `Tracking`(`track_name`, `role`, `datetime`) VALUES(%s, %s, %s)"
        cursor.execute(sql, [req['track_name'], req['role'], datetime.datetime.now()])
        cursor.close()
        connection.commit()
    except:
        return 'error'
    return 'ok'

@app.route('/insertRealtime', methods=['GET', 'POST'])
def insertRealtime():
    '''
        {
            "message": xxx,
            "message_tag": xxx,
            "message_type": xxx,
            "linktosource": xxx,
        }
    '''

    req = __getRequest()
    date = datetime.datetime.now()

    db.collection('Social').document(str(date)).set({
        "datetime": date,
        "message": req['message'],
        "tokenize": preprocess.text_to_tokens(req['message']),
        "linktosource": req['linktosource'],
        'message_type': req['message_type'],
        'message_tag': list(req['message_tag'].split(',')),
        'status': 'no'
    })
    return 'ok'

@app.route('/updateRealtime', methods=['GET', 'POST'])
def updateRealtime():
    '''
        {
            doc: xxx
        }
    '''
    req = __getRequest()

    doc = req['doc']

    db.collection('Social').document(doc['datetime']).set(doc)

    return 'ok'

@app.route('/summaryAll', methods=['GET', 'POST'])
def summaryAll():
    date = datetime.datetime.now()
    docs = __getRealtime()

    # count pos, neg
    AIS_count, DTAC_count, TRUE_count, GLOBAL_count = [0, 0], [0, 0], [0, 0], [0, 0] # [pos_count, neg_count]
    REPLIES_count = 0
    target = 'ais' # TODO select target to count

    def count(tmp):
        if doc['sentiment'].lower() == 'positive':
            tmp[0] += 1
        else:
            tmp[1] += 1
        return tmp

    for doc in docs:
        # count pos, neg
        if 'ais' in map(str.lower, doc['brand']):
            AIS_count = count(AIS_count)
        if 'dtac' in map(str.lower, doc['brand']):
            DTAC_count = count(DTAC_count)
        if 'true' in map(str.lower, doc['brand']):
            TRUE_count = count(TRUE_count)
        GLOBAL_count = count(GLOBAL_count)

        # count replies
        if 'status' in doc:
            if doc['status'] == 'yes':
                REPLIES_count += 1

    # trend
    trend_global = __getTrend()
    trend_AIS    = __getTrend(AIS_word)
    trend_DTAC   = __getTrend(DTAC_word)
    trend_TRUE   = __getTrend(TRUE_word)

    # save summary
    cursor = connection.cursor
    sql = 'INSERT INTO `Summary` VALUES(%s, %s, %s, %s, %s, %s)'
    cursor.execute(sql, [date, 'GLOBAL', REPLIES_count, limit_query, GLOBAL_count[0], GLOBAL_count[1]]) # (date, brand, no_re, no_post, no_pos, no_neg)
    cursor.close()
    connection.commit()
    
    cursor = connection.cursor
    sql = 'INSERT INTO `Summary` VALUES(%s, %s, %s, %s, %s, %s)'
    cursor.execute(sql, [date, 'AIS', REPLIES_count, limit_query, AIS_count[0], AIS_count[1]])
    cursor.close()
    connection.commit()

    cursor = connection.cursor
    sql = 'INSERT INTO `Summary` VALUES(%s, %s, %s, %s, %s, %s)'
    cursor.execute(sql, [date, 'DTAC', REPLIES_count, limit_query, DTAC_count[0], DTAC_count[1]])
    cursor.close()
    connection.commit()

    cursor = connection.cursor
    sql = 'INSERT INTO `Summary` VALUES(%s, %s, %s, %s, %s, %s)'
    cursor.execute(sql, [date, 'TRUE', REPLIES_count, limit_query, TRUE_count[0], TRUE_count[1]])
    cursor.close()
    connection.commit()

    # save trend
    for trend in trend_global:
        cursor = connection.cursor
        sql = 'INSERT INTO `Trend` VALUES(%s, %s, %s, %s)'
        cursor.execute(sql, [date, trend[0], 'GLOBAL', round(float(trend[1]), 2)]) # (date, word, brand, score)
        cursor.close()
        connection.commit()

    for trend in trend_AIS:
        cursor = connection.cursor
        sql = 'INSERT INTO `Trend` VALUES(%s, %s, %s, %s)'
        cursor.execute(sql, [date, trend[0], 'AIS', round(float(trend[1]), 2)])
        cursor.close()
        connection.commit()

    for trend in trend_DTAC:
        cursor = connection.cursor
        sql = 'INSERT INTO `Trend` VALUES(%s, %s, %s, %s)'
        cursor.execute(sql, [date, trend[0], 'DTAC', round(float(trend[1]), 2)])
        cursor.close()
        connection.commit()

    for trend in trend_TRUE:
        cursor = connection.cursor
        sql = 'INSERT INTO `Trend` VALUES(%s, %s, %s, %s)'
        cursor.execute(sql, [date, trend[0], 'TRUE', round(float(trend[1]), 2)])
        cursor.close()
        connection.commit()

    return 'ok'

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000, debug= False if getenv('SERVER_ENV') else True)