# Backend

## Setup

```zsh
# install package
pip3 install -r requirements.txt

# (optional) init sql & phpmyadmin if you have aready skipped this step.
docker-compose -f ./.cd/docker-compose.yml up -d

# start server
python3 app.js
```

This application listening in localhost port `8000`

For api document [here](./API_DOCUMENT.md)
