FROM python:3.7.2

WORKDIR /backend

COPY backend/. /backend

RUN pip install -r requirements.txt

EXPOSE 8000

CMD python app.py
